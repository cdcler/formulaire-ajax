<?php

namespace App\Controller;

use App\Entity\Personne;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
   
    /**
     * @Route("default/", name="personne_success")
     */
    public function insertPersonne(Request $request)
    {
        $personne = new Personne();

        $form = $this->createFormBuilder($personne)
        ->add('civilite', TextType::class)
        ->add('nom', TextType::class)
        ->add('prenom')
        ->add('email')
        ->add('telephone')
        ->add('newsletter')
        ->add('creer', SubmitType::class, ['label' => 'Creer Personne'])
        ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
        $personne = $form->getData();

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($personne);
        $entityManager->flush();

        return $this->redirectToRoute('personne_success');

    }


        return $this->render('default/index.html.twig', [
            'form' => $form->createview(),
        ]);
    
    }



}
