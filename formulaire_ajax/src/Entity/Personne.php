<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass="App\Repository\PersonneRepository")
 */
class Personne
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\Choice({0, 1, -1})
     */
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $civilite;

    /**
     * @Assert\Length(min=2)
     */
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nom;

    /**
     * @Assert\Length(min=2)
     */
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $prenom;

    /**
     * @Assert\Email(
     *     message = "Le email '{{ value }}' n'est pas une adresse mail valide.",
     *     checkMX = true
     * )
     */
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $email;

    /**
     * @Assert\Length(min=10)
     */
    /**
     * @ORM\Column(type="string", length=13)
     */
    private $telephone;


    /**
     * @ORM\Column(type="boolean")
     */
    private $newsletter;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCivilite(): ?string
    {
        return $this->civilite;
    }

    public function setCivilite(?string $civilite): self
    {
        $this->civilite = $civilite;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getNewsletter(): ?bool
    {
        return $this->newsletter;
    }

    public function setNewsletter(bool $newsletter): self
    {
        $this->newsletter = $newsletter;

        return $this;
    }
}
